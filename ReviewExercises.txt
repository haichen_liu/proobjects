#########################################################################
# Use this file to answer Review Exercises from the Big Java textbook
#########################################################################

R8.1 Encapsulation

The process of providing a public interface, while hiding the implementation details, is called encapsulation.

Encapsulation enable changes in the implementation without affecting users of a class.

R8.4 Public interface

The set of all methods provided by a class, together with a description of their behavior, is called the public
interface of the class.

When using the public interface of a class, users do not need to know and the implementation of this class.

R8.7 Instance versus static

Instance methods are used to mutate or access to an object.

Instance methods can access to instance variables of a class, and static method cannot be called by an object, but a
class.

R8.8 Mutator and accessor

Mutators are some interfaces for users to modify variables in a class.

Accessors are some interfaces for users to get the values of variables in a class.

R8.9 Implicit parameter

The explicit parameter is set to the argument.

The implicit parameter references the object.

R8.10 Implicit parameter

Only one implicit parameter, and as many explicit parameters as are required.

R8.12 Constructors

One constructor as default, and as many as are required.

If a class has no constructor, a default constructor will be provided with no parameters and initiate all variables as

their default values.

Determined by their parameters' types.

R8.16 Instance variables

According to the principle of encapsulation, variable should be set private to prevent from user changing some parameters
and ignoring the logic relationship of others. This may cause some problems.

R8.19 The this reference

This reference refers to the class it belongs to.

R8.20 Zero, null, false, empty String

Zero is the default value of an integer.

Null is the default value of a class.

False is the default value of a boolean.

Empty string is the default value of a string.




