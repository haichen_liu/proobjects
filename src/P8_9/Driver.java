package P8_9;

/**
 * Created with IntelliJ IDEA.
 * User: haichen
 * Date: 10/17/13
 * Time: 1:33 PM
 * To change this template use File | Settings | File Templates.
 */
public class Driver {
    public static void main(String[] args) {
        ComboLock comboLock = new ComboLock(1, 12, 33);
        comboLock.reset();
        comboLock.turnRight(1);
        comboLock.turnLeft(12);
        comboLock.turnRight(33);
        if (comboLock.open()){
            System.out.println("OPEN!");
        }
    }
}
