package P8_9;

/**
 * Created with IntelliJ IDEA.
 * User: haichen
 * Date: 10/17/13
 * Time: 1:17 PM
 * To change this template use File | Settings | File Templates.
 */
public class ComboLock {
    private int[] dSecret;
    private int current;
    private boolean isCorrect;

    public ComboLock(int secret1, int secret2, int secret3) {
        dSecret = new int[3];
        dSecret[0] = secret1;
        dSecret[1] = secret2;
        dSecret[2] = secret3;
        for (int i = 0; i < 3; i++) {
            if (dSecret[i] < 0 || dSecret[i] > 39){
                System.out.println("Illegal Input!");
                return;
            }
        }
    }

    public void reset(){
        current = 0;
        isCorrect = false;
    }

    public void turnRight(int ticks){
        if (current == 0){
            if (ticks == dSecret[0]){
                isCorrect = true;
            }
        }else {
            if (isCorrect && ticks == dSecret[2] && current == 2){
                isCorrect = true;
            }else {
                isCorrect = false;
            }
        }
        current ++;

    }

    public void turnLeft(int ticks){
        if (current == 1){
            if (isCorrect && ticks == dSecret[1]){
                isCorrect = true;
            }
        }else {
            isCorrect = false;
        }

        current ++;

    }

    public boolean open(){
        if (isCorrect){
            reset();
            return true;
        }else {
            return false;
        }

    }
}
