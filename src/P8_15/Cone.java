package P8_15;

/**
 * Created with IntelliJ IDEA.
 * User: haichen
 * Date: 10/17/13
 * Time: 2:23 PM
 * To change this template use File | Settings | File Templates.
 */
public class Cone extends Geometry{
    public static double Volume(double r, double h){
        return Math.PI*r*r*h/3;
    }

    public static double Surface(double r, double h){
        return Math.PI*r*Math.sqrt(h*h+r*r)+Math.PI*r*r;
    }
}
