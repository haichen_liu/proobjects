package P8_15;

/**
 * Created with IntelliJ IDEA.
 * User: haichen
 * Date: 10/17/13
 * Time: 2:18 PM
 * To change this template use File | Settings | File Templates.
 */
public class Geometry {

    public static double sphereVolume(double r){
        return 4/3*Math.PI*r*r*r;
    }

    public static double sphereSurface(double r){
        return 4*Math.PI*r*r;
    }

    public static double cylinderVolme(double r, double h){
        return Math.PI*r*r*h;
    }

    public static double cylinderSurface(double r, double h){
        return Math.PI*r*2.0*h+2.0*Math.PI*r*r;
    }

    public static double coneVolume(double r, double h){
        return Math.PI*r*r*h/3;
    }

    public static double coneSurface(double r, double h){
        return Math.PI*r*Math.sqrt(h*h+r*r)+Math.PI*r*r;
    }
}
