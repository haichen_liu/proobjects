package P8_15;

/**
 * Created with IntelliJ IDEA.
 * User: haichen
 * Date: 10/17/13
 * Time: 2:20 PM
 * To change this template use File | Settings | File Templates.
 */
public class Sphere extends Geometry{
    public static double volume(double r){
            return 4/3*Math.PI*r*r*r;
    }

    public static double surface(double r){
        return 4*Math.PI*r*r;
    }
}
