package P8_15;

/**
 * Created with IntelliJ IDEA.
 * User: haichen
 * Date: 10/17/13
 * Time: 2:22 PM
 * To change this template use File | Settings | File Templates.
 */
public class Cylinder extends Geometry{
    public static double Volume(double r, double h){
        return Math.PI*r*r*h;
    }

    public static double Surface(double r, double h){
        return Math.PI*r*2.0*h+2.0*Math.PI*r*r;
    }
}
