package P8_15;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * User: haichen
 * Date: 10/17/13
 * Time: 2:24 PM
 * To change this template use File | Settings | File Templates.
 */
public class Drive {
    public static void main(String[] args) {
        System.out.println("Please enter two numbers:");
        Scanner scanner = new Scanner(System.in);
        double[] para = new double[2];
        //String input = scanner.nextLine();
        for (int i = 0; i < 2; i++) {
            para[i] = scanner.nextDouble();
        }

        System.out.printf("r = %.3f, h = %.3f.",para[0], para[1]);
        System.out.println();
        System.out.printf("The volume of sphere is %f.", Sphere.volume(para[0]));
        System.out.println();
        System.out.printf("The surface of sphere is %f.", Sphere.surface(para[0]));
        System.out.println();
        System.out.printf("The volume of cylinder is %f.", Cylinder.Volume(para[0], para[1]));
        System.out.println();
        System.out.printf("The surface of cylinder is %f.", Cylinder.Surface(para[0], para[1]));
        System.out.println();
        System.out.printf("The volume of cone is %f.", Cone.Volume(para[0], para[1]));
        System.out.println();
        System.out.printf("The surface of cone is %f.", Cone.Surface(para[0], para[1]));
        System.out.println();
        System.out.println("__________________________________");
        System.out.println("This way is more object-oriented!");

    }
}
