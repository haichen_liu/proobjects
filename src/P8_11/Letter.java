package P8_11;

/**
 * Created with IntelliJ IDEA.
 * User: haichen
 * Date: 10/17/13
 * Time: 1:43 PM
 * To change this template use File | Settings | File Templates.
 */
public class Letter {
    private String from;
    private String to;
    private String Text;

    public Letter(String from, String to) {
        this.from = from;
        this.to = to;
        Text = " ";
    }

    public void addLine(String line){
        this.Text += "\n"+line;
    }

    public String getTex(){
        return "Dear "+to+":\n"+this.Text+"\n\n"+"Sincerely,\n\n"+this.from+"\n";
    }

    public void main() {
        System.out.println(this.getTex());
    }
}
