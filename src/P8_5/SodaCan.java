package P8_5;

/**
 * Created with IntelliJ IDEA.
 * User: haichen
 * Date: 10/17/13
 * Time: 12:05 PM
 * To change this template use File | Settings | File Templates.
 */
public class SodaCan {

    private double dHeight;
    private double dRadius;

    public SodaCan() {
    }

    public SodaCan(double dHeight, double dRadius) {
        if (dHeight<=0||dRadius<=0){
            System.out.println("Illegal Set!");
            return;
        }
        this.dHeight = dHeight;
        this.dRadius = dRadius;
    }

    public void setdHeight(double dHeight) {
        if (dHeight<=0){
            System.out.println("Illegal Set!");
            return;
        }
        this.dHeight = dHeight;
    }

    public void setdRadius(double dRadius) {
        if (dRadius<=0){
            System.out.println("Illegal Set!");
            return;
        }
        this.dRadius = dRadius;
    }

    public double getdHeight() {
        return dHeight;
    }

    public double getdRadius() {
        return dRadius;
    }

    public double getSurfaceArea(){
        if (dHeight<=0 || dRadius<=0){
            System.out.println("Please set the height and radius!");
            return 0;
        }
        return Math.PI*dRadius*2.0*dHeight+2.0*Math.PI*dRadius*dRadius;
    }

    public double getVolume(){
        if (dHeight<=0 || dRadius<=0){
            System.out.println("Please set the height and radius!");
            return 0;
        }
        return Math.PI*dRadius*dRadius*dHeight;
    }
}
