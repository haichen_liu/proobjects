package P8_6;

/**
 * Created with IntelliJ IDEA.
 * User: haichen
 * Date: 10/17/13
 * Time: 12:18 PM
 * To change this template use File | Settings | File Templates.
 */
public class Car {
    private double fEfficiency;
    private double fGasLevel;
    private double fDistance;

    public Car() {
    }

    public Car(double fEfficiency) {
        this.fEfficiency = fEfficiency;
    }

    public void addGas(double newGas){
        fGasLevel += newGas;
    }

    public void drive(double mile){
        fGasLevel -= mile*fEfficiency;
    }

    public double getfGasLevel() {
        return fGasLevel;
    }
}
