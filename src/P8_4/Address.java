package P8_4;

/**
 * Created with IntelliJ IDEA.
 * User: haichen
 * Date: 10/17/13
 * Time: 11:44 AM
 * To change this template use File | Settings | File Templates.
 */
public class Address {
    private int dAptNum;
    private int dHouseNum;
    private String sZipCode;
    private String sStreet;
    private String sCity;
    private String sState;

    public Address() {
    }

    public Address(int dHouseNum, String sZipCode, String sStreet, String sCity, String sState) {
        this.dHouseNum = dHouseNum;
        this.sZipCode = sZipCode;
        this.sStreet = sStreet;
        this.sCity = sCity;
        this.sState = sState;
    }

    public Address(int dAptNum, int dHouseNum, String sZipCode, String sStreet, String sCity, String sState) {
        this.dAptNum = dAptNum;
        this.dHouseNum = dHouseNum;
        this.sZipCode = sZipCode;
        this.sStreet = sStreet;
        this.sCity = sCity;
        this.sState = sState;
    }

    public void print(){
        if (dAptNum != 0)
            System.out.printf("Apt %d, ", dAptNum);
        System.out.printf("%d, %s", dHouseNum, sStreet);
        System.out.println();
        System.out.printf("%s, %s, %s", sCity, sState, sZipCode);
        System.out.println();
    }

    public boolean comesBefore(Address other){
        int dZipCode1 = Integer.parseInt(this.sZipCode);
        int dZipCode2 = Integer.parseInt(other.sZipCode);
        return (dZipCode1<dZipCode2);
    }
}
