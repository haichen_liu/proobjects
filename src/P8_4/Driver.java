package P8_4;

/**
 * Created with IntelliJ IDEA.
 * User: haichen
 * Date: 10/17/13
 * Time: 11:59 AM
 * To change this template use File | Settings | File Templates.
 */
public class Driver {
    public static void main(String[] args) {
        Address address;

        address = new Address(3, 849, "60615", "E, 52nd Street", "Chicago", "IL");

        address.print();

        address = new Address(849, "60615", "E, 52nd Street", "Chicago", "IL");

        address.print();
    }
}
