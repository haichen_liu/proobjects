package P8_8;

/**
 * Created with IntelliJ IDEA.
 * User: haichen
 * Date: 10/17/13
 * Time: 12:38 PM
 * To change this template use File | Settings | File Templates.
 */
public class Student {
    private String sName;
    private int dScore;
    private int dCount;
    private double mGPA;
    private String sGrade;

    public Student(String sName) {
        this.sName = sName;
    }

    public String getsName() {
        return sName;
    }

    public void addQuiz(int dScore){
        this.dScore += dScore;
        this.dCount++;
    }

    public int getTotalScore(){
        return dScore;
    }

    public double getAverageScore(){
        return dScore/dCount/1.0;
    }


    public double getGPA(){
        mGPA = (getAverageScore()-60)/10;
        if (mGPA<=0){
            return 0;
        }
        return mGPA;
    }

    public String getsGrade(){

        double temp = mGPA;
        if (temp == 0){
            sGrade = "F";
        }else if (temp>0 && temp<=1.7){
            sGrade = "D";
        }else if (temp>1.7 && temp<=2){
            sGrade = "C-";
        }else if (temp>2 && temp<=2.3){
            sGrade = "C";
        }else if (temp>2.3 && temp<=2.7){
            sGrade = "C+";
        }else if (temp>2.7 && temp<=3){
            sGrade = "B-";
        }else if (temp>3 && temp<=3.3){
            sGrade = "B";
        }else if (temp>3.3 && temp<=3.7){
            sGrade = "B+";
        }else if (temp>3.7 && temp<4){
            sGrade = "A-";
        }else if (temp == 4){
            sGrade = "A";
        }

        return sGrade;
    }

}

