package P8_8;

/**
 * Created with IntelliJ IDEA.
 * User: haichen
 * Date: 10/17/13
 * Time: 1:09 PM
 * To change this template use File | Settings | File Templates.
 */
public class Driver {
    public static void main(String[] args) {
        Student student = new Student("Tom");
        student.addQuiz(100);
        student.addQuiz(90);
        student.addQuiz(80);
        student.addQuiz(70);
        System.out.println(student.getAverageScore());
        System.out.println(student.getGPA());
        System.out.println(student.getsGrade());
    }
}
